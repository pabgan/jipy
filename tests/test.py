#!/bin/env python3
import unittest
import ji
from jira import JIRA

class TestJipyMethods(unittest.TestCase):

    def test_issue(self):
        issue = ji.issue('DSLE-24870', fields=['SUMMARY','UPDATED','ASSIGNEE','STATUS'])
        self.assertIsNotNone(issue)

        summary = '[New GUI] OLT Traffic: OLT ports are being loaded twice'
        self.assertEqual(issue.fields.summary, summary)
        assignee = 'Pablo Ganuza'
        self.assertEqual(assignee, issue.fields.assignee.displayName)

#    def test_isupper(self):
#        self.assertTrue('FOO'.isupper())
#        self.assertFalse('Foo'.isupper())
#
#    def test_split(self):
#        s = 'hello world'
#        self.assertEqual(s.split(), ['hello', 'world'])
#        # check that s.split fails when the separator is not a string
#        with self.assertRaises(TypeError):
#            s.split(2)

if __name__ == '__main__':
    unittest.main()

