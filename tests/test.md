Apparently by default, a number of fields are being added to `getAvailableFields` response in `PE_DATA`, but actually they do not apply to all reports.

That is the case for example in `pedata/v1/reports/SERVICE_RECOMMENDATION_DATA_BY_TBOX/fields`:

```xml
{
  "statusCode": 0,
  "message": null,
  "data": [
    {
      "FIELD_NAME": "service.recommender.tbox.num_lines"
    },
    {
      "FIELD_NAME": "service.recommender.tbox.serviceproduct.most_used"
    },
    {
      "FIELD_NAME": "lineinfo.id"
    },
    {
      "FIELD_NAME": "dateinfo.data_collection_date"
    },
    {
      "FIELD_NAME": "dateinfo.estimation_date"
    },
    {
      "FIELD_NAME": "service.recommender.tbox.recommendation.x.percentile"
    },
    {
      "FIELD_NAME": "service.recommender.tbox.serviceproduct.group.evaluated"
    },
    {
      "FIELD_NAME": "service.recommender.tbox.num_lines.with_service_recommendation"
    },
    {
      "FIELD_NAME": "service.recommender.tbox.recommendation.x"
    },
    {
      "FIELD_NAME": "service.recommender.tbox.serviceproduct.most_used.num_lines"
    },
    {
      "FIELD_NAME": "lineinfo.port"
    },
    {
      "FIELD_NAME": "service.recommender.recommendation.equipmentupgrade"
    },
    {
      "FIELD_NAME": "lineinfo.dslam"
    },
    {
      "FIELD_NAME": "lineinfo.id2"
    },
    {
      "FIELD_NAME": "service.recommender.tbox.id.x"
    }
  ]
}
```

Those `lineinfo` fields probably do not make sense in this kind of report.

@ialmandoz
 @fgarzas
@none
