#!/bin/sh
# This script will get all the issue's information and create a 
# markdown file with it.
# Still lot TODO
issue=${1:-$TASK}
dir=${2:-.ticket}

mkdir -p $dir
cd $dir
ji.py query --issue $issue --fields all > all.raw

while read line
do
	field_name=$(echo $line | cut -d'=' -f1 )
	field_value=$(echo $line | cut -d'=' -f2- | sed -E 's/<EOL>/\n/g')
	echo $field_value > $field_name.txt
done < all.raw

for f in $(ls *.txt); do
	echo "#### ${f%.txt}" >> ../ticket.md
	cat $f >> ../ticket.md
done
cd ..
