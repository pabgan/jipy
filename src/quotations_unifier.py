#!/usr/bin/python3
import argparse

########################################################
### CONFIGURATION
########################################################

########################################################
### ACTIONS
########################################################

########################################################
### MAIN
########################################################
if __name__ == "__main__":
    desc= "Remove tags separated by a new line"
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument("tag", type=str)
    parser.add_argument("file", type=str)
    args = parser.parse_args()

    with open(args.file, 'r') as text:
        for line in text:
            line = line.strip()
            if line.startswith(args.tag):
                multiline = True

                print(args.tag)
                print(line[len(args.tag):- (len(args.tag))])

                while multiline:
                    next_line = text.readline().strip()

                    if not next_line.startswith(args.tag):
                        print(args.tag)
                        print(next_line)
                        multiline = False
                    else:
                        print(next_line[len(args.tag):- (len(args.tag))])

            else:
                print(line)
