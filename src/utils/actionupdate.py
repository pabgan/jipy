import os, logging
from jira import JIRA
import utils.config as config
import utils.utils as utils

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(os.path.basename(__file__))
#logger.setLevel(logging.DEBUG)

########################################################
### ASSIGN
########################################################
def assign(issueid, user):
    logger.debug("Assigning {} to {}".format(issueid, user))

    if not config.jira.assign_issue(issueid, assignee=user):
        logger.error("Assigning issue {} to user {} failed.".format(issueid, user))


########################################################
### ATTACH
########################################################
def attach(issueid, attachments):
    if attachments is None:
        logger.error("Missing files to attach.")
        exit(2)

    files = attachments.split()

    for file in files:
        if not os.path.exists(file):
            logger.error("{} is not a file".format(file))
        else:
            logger.debug("Attaching {} to {}".format(file, issueid))
            print(config.jira.add_attachment(issueid, file))


########################################################
### COMMENT
########################################################
def comment(issueid, commenttext, commentid=None):
    if not commenttext:
        logger.error("Missing text to comment.")
        exit(2)

    if not commentid:
        # We are adding a new comment here
        logger.debug("Adding comment to {}: \"{}\"".format(issueid, commenttext))
        print(config.jira.add_comment(issueid, commenttext))
    else:
        # We are updating a comment
        comment = config.jira.comment(issueid, commentid)
        comment.update(body=commenttext)


########################################################
### TRANSITION
########################################################
def transition(issueid, transitionstr=None, assignee=None, commenttext=None):
    chosentransition = _choosetransition(issueid, transitionstr)
    logger.debug("Chosen transition: {}".format(chosentransition))

    if chosentransition is not None:
        if assignee is not None:
            assign(issueid, assignee)

        config.jira.transition_issue(issueid, chosentransition, comment=commenttext)
    else:
        logger.error("No transition chose.")

def _choosetransition(issueid, ttype=None):
    choice = None
    logger.debug("Gathering available transitions for {}".format(issueid))
    transitions = config.jira.transitions(issueid)

    if transitions is None:
        logger.error("No available transitions.")
        return None

    logger.debug("Available transitions for {}".format(transitions))
    if ttype is not None:
        logger.debug("Filtering by {}".format(ttype))
        filteredtransitions = list(filter(lambda transition: ttype.lower() in transition['name'].lower(), transitions))
    else:
        filteredtransitions = transitions

    logger.debug("Transitions filtered: {}".format(ttype, filteredtransitions))

    # At this point there are transitions available
    if len(filteredtransitions) == 0:
        # If no transitions make the filter, maybe the user made a type
        # let him chose manually
        logger.warn("No available transitions after filtering. Asking user to chose manually from all transitions available.")
        choice = _userchosestransition(transitions)
    elif len(filteredtransitions) == 1:
        # The user provided a filter and it was so accurate that only one transition
        # makes the cut, so we will chose it
        logger.debug("Only one suitable transition found: {}".format(filteredtransitions[0]))
        choice = filteredtransitions[0]['id']
        logger.debug("Automatically chosing: {}".format(choice))
    else:
        logger.debug("More than one transition available after filtering. Asking user to chose manually from all transitions available.")
        choice = _userchosestransition(filteredtransitions)

    return choice

def _userchosestransition(transitions):
    print("Please chose a transition by writing its ID:")

    for transition in transitions:
        print("ID: {}".format(transition['id']))
        print("Name: {}".format(transition['name']))
        print("Description: {}".format(transition['to']['description']))
        print("-------------------------------------------")

    choice = input("\nType ID = ")

    logger.debug("User chose: {}".format(choice))

    return choice


########################################################
### FIELD
########################################################
def updatefields(issueid, updates):
    logger.debug("Updating {}.{}={}".format(issueid, field, value))

    # TODO
    fieldname = utils.getactualfieldname(field)

    updates= {field: value}

    issue = config.jira(issueid)
    return issue.update(fields=updates)
