import logging
import utils.config as config


# TODO: accept raw field name
def getactualfieldname(prettyfieldname):
    return config.customfields.get(prettyfieldname, prettyfieldname)

# TODO: accept raw field name
def getfieldvalue(issue, prettyfieldname):
    value = None
    logging.debug("Extracting '{}' from {}".format(prettyfieldname, issue.raw))

    actualfieldname = getactualfieldname(prettyfieldname)
    logging.debug("Translated '{}' into '{}'".format(prettyfieldname, actualfieldname))

    value = getattr(issue.fields, actualfieldname)
    logging.debug("Got value {}='{}'".format(prettyfieldname, value))

    if 'fixVersions' in prettyfieldname:
        values = [version.name for version in value]
        return values

    return value
