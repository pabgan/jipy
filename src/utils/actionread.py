import os, sys, logging
from csv2md.table import Table
from jira import JIRA, JIRAError
import pprint
import utils.config as config
import utils.utils as utils


def issue(issueid, fields, format, header):
    fieldstoshow = fields.split(',')
    try:
        issue = config.jira.issue(issueid)
    except JIRAError as error:
        sys.exit(error.text)

    logging.debug("Requested fields to show: {}".format(fieldstoshow))
    logging.debug("All keys should be: {}".format(vars(issue.fields)))

    if fieldstoshow == ["all"]:
        pprint.pprint(vars(issue.fields))
        return issue

    if len(fieldstoshow) == 1:
        try:
            print(utils.getfieldvalue(issue, fieldstoshow[0]))
        except AttributeError:
            logging.warning("Issue does not have field '{}({})'".format(fieldstoshow[0], config.customfields.get(fieldstoshow[0])))
    else:
        if format == 'md':
            table = list()
            for field in fieldstoshow:
                table.append("{}{}{}".format(field, config.CSV_DELIMITER, utils.getfieldvalue(issue, field)))

            print(Table.parse_csv(table, delimiter=config.CSV_DELIMITER).markdown())
        else:
            for field in fieldstoshow:
                print("{}={}".format(field, utils.getfieldvalue(issue, field)))

    return issue


def query(query, fields, format, print_header):
    fieldstoshow = fields.split(',')
    issues = config.jira.search_issues(query, maxResults=500)
    logging.debug("issues={}".format(issues))

    table = list()

    if print_header:
        table.append('key' + config.CSV_DELIMITER + config.CSV_DELIMITER.join(fieldstoshow))

    for issue in issues:
        field_values = config.CSV_DELIMITER.join([str(utils.getfieldvalue(issue, field)) for field in fieldstoshow])
        row = issue.key + config.CSV_DELIMITER + field_values
        table.append(row)

    if format == 'md':
        print(Table.parse_csv(table, delimiter=config.CSV_DELIMITER).markdown())
    elif format == 'plain':
        print(Table.parse_csv(table, delimiter=config.CSV_DELIMITER).markdown().replace('|', ' '))
    else:
        if len(table) > 0:
            print('\n'.join(table))

    return issues


def getversions(project):
    logging.debug("Getting all Fix Versions defined for project {}".format(project))
    prj = config.jira.project(project)

    for fv in prj.versions:
        print(fv)

