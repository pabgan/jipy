import os
from jira import JIRA

# TODO: Use configparser to read all configuration, including pretty names for custom fields

########################################################
### CONFIGURATION
########################################################
jira = JIRA('https://dzsi.atlassian.net/')
customfields_file_path = os.path.join(os.getenv("HOME"), '.config', 'jipy', 'customfields.cfg')
CSV_DELIMITER = ';'

customfields = dict()

with open(customfields_file_path) as customfields_file:
    for row in customfields_file:
        pretty_name, actual_name = row.split('=')
        customfields[pretty_name.strip()] = actual_name.strip()

