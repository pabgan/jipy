#!/bin/zsh
# See [1] on the need for this
setopt NULL_GLOB

INPUT=${1:-/dev/stdin}
output=$(cat $INPUT)

# Remove secret lines
output=$( echo "${output}" | sed -E '/^\_#/d' )

# Convert to Jira
output=$( echo "${output}" | pandoc -t jira )
## Fix translation
### Add blank lines before and after header definitions, or they won't be interpreted
output=$( echo "${output}" | sed -E 's/^(h[[:digit:]].*)/\n\1\n/g')

# Convert usernames
source ${jira_users_list}
## Grep out all the words starting with '@' and containing only letters
for name in $( echo "${output}" | grep -oE '@[[:alpha:]_]+' ); do
	## For each one of those names, look for its Jira ID
	name_without_at=${name#@}
	name_substituted=${(P)name_without_at}
	
	## Only if the Jira ID is found, substitute the name with it
	if [ "$name_substituted" != '' ]; then
		output=$( echo "${output}" | sed -E "s/$name/$name_substituted/g" )
	fi
done

# Unescape the sequence that links to attachments
output=$( echo $output | sed -E 's/\\\[\\\^([^]]+)\\]/[^\1]/g' )
# TODO: report bug to pandoc
# ± $ pandoc --from markdown --to jira <(echo '[^target-with-changes-fail.tar.gz].')
# \[\^target-with-changes-fail.tar.gz].
output=$( echo $output | sed -E 's/\\\[\\\^([^]]+)]/[^\1]/g' )

# Add color to results
## After Jira conversion by Pandoc, those lines look like:
## h4. {anchor:pass-2}PASS
output=$( echo $output | sed 's/}PASS$/}{color:green}PASS{color}/g' )
output=$( echo $output | sed 's/}DONE$/}{color:green}DONE{color}/g' )
output=$( echo $output | sed 's/}OK$/}{color:green}OK{color}/g' )
output=$( echo $output | sed 's/}PASS?$/}{color:orange}PASS?{color}/g' )
output=$( echo $output | sed 's/}DONE?$/}{color:orange}DONE?{color}/g' )
output=$( echo $output | sed 's/}OK?$/}{color:orange}OK?{color}/g' )
output=$( echo $output | sed 's/}FAIL$/}{color:red}FAIL{color}/g' )
output=$( echo $output | sed 's/}FAIL?$/}{color:orange}FAIL?{color}/g' )
output=$( echo $output | sed 's/}BLOCKED$/}{color:orange}BLOCKED{color}/g' )
output=$( echo $output | sed 's/}SKIPPED$/}{color:grey}SKIPPED{color}/g' )
output=$( echo $output | sed 's/^* \\\[OK\\\]/* \\[{color:green}OK{color}\\]/g')
output=$( echo $output | sed 's/^* \\\[DONE\\\]/* \\[{color:green}DONE{color}\\]/g')
output=$( echo $output | sed 's/^* \\\[KO\\\]/* \\[{color:red}KO{color}\\]/g')
output=$( echo $output | sed 's/^* \\\[NA\\\]/* \\[{color:grey}NA{color}\\]/g')
output=$( echo $output | sed 's/^* \\\[pending\\\]/* \\[{color:orange}pending{color}\\]/g')
output=$( echo $output | sed 's/^* \\\[ongoing\\\]/* \\[{color:magenta}ongoing{color}\\]/g')

echo $output

unsetopt NULL_GLOB

# HELP
# [1] https://www.bartbusschots.ie/s/2019/06/12/bash-to-zsh-file-globbing-and-no-matches-found-errors/
