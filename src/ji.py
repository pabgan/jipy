#!/usr/bin/python3
import argparse, os, logging
import json
from utils.actionupdate import assign, attach, comment, transition, updatefields
from utils.actionread import issue, query, getversions
import utils.config as config

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(os.path.basename(__file__))
#logger.setLevel(logging.DEBUG)


########################################################
### AUXILIAR FUNCTIONS
########################################################
def _read_comment(comment):
    text = None

    if os.path.exists(comment):
        logger.debug("{} is a file".format(comment))
        with open(comment, 'r') as cf:
            text = cf.read()
    else:
        text = comment

    if "/proc/self/fd" in text:
        logger.error("{} was found not to be a file, but contained the name of what looked like a file descriptor.".format(comment))
        text = None

    return text

########################################################
### ACTIONS
########################################################
def update(args):
    logger.debug(">>>update({})".format(args))

    commenttext = None

    if args.comment:
        commenttext = _read_comment(args.comment)

    if args.attach:
        logger.debug("Processing attach request...")
        attach(args.issue, args.attach)

    if args.transition:
        logger.debug("Processing transition request...")
        transition(args.issue, args.transition, args.assign, commenttext)
        return
    elif args.assign_to_me:
        logger.debug("Processing assignment-to-self request...")
        userme = str(config.jira.user(config.jira.current_user()))
        assign(args.issue, userme)
    elif args.assign:
        logger.debug("Processing assignment request...")
        assign(args.issue, args.assign)

    if args.values:
        logger.debug("Processing update values...")
        updates = json.loads(args.values)
        updatefields(args.issue, updates)

    if commenttext:
        logger.debug("Processing comment request...")
        comment(args.issue, commenttext, args.comment_id)


def create():
    logger.warning("Action not implemented.")

def read(args):
    if args.issue:
        issue(args.issue, args.fields, args.format, args.header)
    elif args.query:
        query(args.query, args.fields, args.format, args.header)
    elif args.versions:
        getversions(args.versions)

def delete():
    logger.warning("Action not implemented.")


########################################################
### MAIN
########################################################
actions = {'create' : create,
           'read'   : read,
           'update' : update,
           'delete' : delete,
}


def main(args):
    actions[args.action](args)


if __name__ == '__main__':
    ###########################################################
    # Parse arguments
    desc= "Interact with Jira"
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument("--log", help="Select log level (DEBUG, INFO, WARNING, ERROR, CRITICAL).")
    parser.add_argument("action", choices=actions.keys(), help="One of {}".format(', '.join(actions.keys())))

    # READ options
    parser.add_argument("--query", type=str, help="A query to search for issues.")
    parser.add_argument("--issue", type=str, help="The issue target to this command.")
    parser.add_argument("--versions", type=str, help="Displays the list of version of the project.")
    parser.add_argument("--fields", default='summary,updated,assignee,status', help='List of comma separated values')
    parser.add_argument("-f", "--format", default='plain', choices=["md", "csv", "plain"], required=False, help="Format in which the output is desired. Currently supported: markdown, CSV, plain text (default)")
    parser.add_argument("--header", default=True)
    parser.add_argument("--split-in-files", action='store_true')

    # UPDATE options
    parser.add_argument("--transition", type=str, help="Text that tries to match a transition's name")
    parser.add_argument("--assign-to-me", action="store_true", help="Text that tries to match a user's name")
    parser.add_argument("--assign", type=str, help="Text that tries to match a user's name")
    parser.add_argument("--comment", type=str, help="Text to add as a comment")
    parser.add_argument("--comment-id", type=str, help="ID of the comment to update")
    parser.add_argument("--attach", type=str, help="List of space separated files to attach to a ticket")
    parser.add_argument("--values", type=str, help="List of space separated files to attach to a ticket")

    args = parser.parse_args()

    if args.log:
        logger.setLevel(level=args.log)
        logger.debug('Setting log level to {}.'.format(args.log))

    main(args)
