# ji.py

A CLI to interact with a Jira instance, written in Python.

UNIX philosophy, easy and simple. Using [this JIRA python library](https://pypi.org/project/jira/).

It is a Work In Progress and many things are still hardcoded to my very specific
needs.


# Installation

WIP


## Dependencies
You need installed the following:

- `python3`
- [this JIRA python library](https://pypi.org/project/jira/) **[configured with your credentials](https://jira.readthedocs.io/examples.html?highlight=netrc#authentication)**.
	- This can be done via `~/.netrc` like:

		```
		machine xxxxxxxxx.atlassian.net
		login xxxxxxxxxxxx@xxxx.com
		password XXXXXXXXXXXXXXXXXXXXXXXX
		```

- [csv2md](https://github.com/lzakharov/csv2md/) for the option `--format md`
- ??

# Examples

## Assign

- Assign ticket to current user:

```sh
pabgan@manoliTo ~/repos/diario [main *]
  $ ji.py update --issue $ISSUE_ID

```

- Assign ticket to specific user:

```sh
pabgan@manoliTo ~/repos/diario [main *]
  $ ji.py update --issue $ISSUE_ID --assign 'pabgan'

```

## Issue

- Get some issue's field:

```sh
pabgan@manoliTo ~/repos/diario [main *]
  $ ji.py read --issue $ISSUE_ID --fields 'summary'
Fix this and that

pabgan@manoliTo ~/repos/diario [main *]
  $ ji.py read --issue $ISSUE_ID --fields 'summary,description'
summary=Fix this and that
description=When testing we found the issue
```

- Get all of some issue's field:

```sh
pabgan@manoliTo ~/repos/diario [main *]
  $ ji.py read --issue $ISSUE_ID --fields 'all'
aggregateprogress=<jira.resources.PropertyHolder object at 0x7ff5bf3c3730>
aggregatetimeestimate=None
aggregatetimeoriginalestimate=None
aggregatetimespent=None
assignee=Gabriel Montes
attachment=[<JIRA Attachment: filename='diff_dsl-expresse-dt-21.3.0-install-Vs-dsl-expresse-dt-22.1.0-install', id='82047', mimeType='text/plain'>, <JIRA Attachment: filename='diff_tmatrix.diff.gz', id='82024', mimeType='application/x-gzip'>, <JIRA Attachment: filename='log_verification.log', id='82566', mimeType='text/plain'>]
comment=<JIRA UnknownResource at 140693452109904>
components=[<JIRA Component: name='Delivery DT', id='13531'>]
created=2021-12-21T11:51:24.521+0100
creator=Clemente Rodríguez
proposed_solution=None
customfield_10001=None
customfield_10002=P2
module_fix_for_version=None
customfield_10039=None
unit_test=None
customfield_10170=None
customfield_10470=None
customfield_10471=None
customfield_10475=0|i09zun:
customfield_10700={}
customfield_10714=<jira.resources.PropertyHolder object at 0x7ff5bf3c37c0>
customfield_10715=2022-02-09T10:58:10.622+0100
customfield_10716=1_*:*_1_*:*_3545750636_*|*_5_*:*_1_*:*_0_*|*_10001_*:*_1_*:*_2755531997
customfield_10717=[]
description=Ticket for tracking the Release V22.1 - DT validation
duedate=None
environment=None
fixVersions=[<JIRA Version: name='V22.1 - DT', id='15504'>]
issuelinks=[]
issuerestriction=<jira.resources.PropertyHolder object at 0x7ff5bf398eb0>
issuetype=Task
labels=[]
lastViewed=2022-03-17T08:11:59.532+0100
priority=Minor
progress=<jira.resources.PropertyHolder object at 0x7ff5bf3c3820>
project=DSLE
reporter=Clemente Rodríguez
resolution=Fixed
resolutiondate=2022-03-04T10:12:47.005+0100
security=None
status=Resolved
statuscategorychangedate=2022-03-04T10:12:47.129+0100
subtasks=[]
summary=V22.1 - DT Release Validation Ticket
timeestimate=None
timeoriginalestimate=None
timespent=None
timetracking=<JIRA TimeTracking at 140693452108800>
updated=2022-03-05T10:10:10.000+0100
versions=[<JIRA Version: name='V21.3 - DT', id='15293'>]
watches=<JIRA Watchers at 140693451932864>
worklog=<jira.resources.PropertyHolder object at 0x7ff5bf3c3880>
workratio=-1
```

- Update some issue's field:

TODO

## Issues

- Search for a list of issues:

```sh
pabgan@manoliTo ~/repos/work/customer-release-testing [master]
  $ ji.py read --query 'project = DSLE AND fixVersion in ( "V22.1 - DT" ) and status=resolved '
key;summary;updated;assignee;status
DSLE-24513;Login does not work;2022-02-24T14:21:33.925+0100;QA;Resolved
DSLE-24412;Logout does not work;2022-02-14T11:25:59.353+0100;Dev;Resolved
DSLE-24406;We should improve everything;2022-03-04T15:25:50.166+0100;QA;Resolved
```

- Format it in markdown:

```md
pabgan@manoliTo ~/
± $ ji.py read -f md --query 'text~"ansible grafana"'
| key        | summary                        | updated                      | assignee  | status   |
| ---------- | ------------------------------ | ---------------------------- | --------- | -------- |
| DSLE-25027 | Do this and that               | 2022-08-25T10:56:50.371+0200 | QA 1      | Closed   |
| DSLE-24796 | And  do not forget about those | 2022-09-19T11:04:46.894+0200 | DEV 1     | Open     |
```


## Comments

The output is the ID of the comment.

- Add a comment:

```sh
pabgan@manoliTo ~/
  $ ji.py update --issue DSLE-25279 --comment 'OK, sorry about that. I will create the right simulator to test that.'
1234876

```

- Add a comment from a file:

```sh
pabgan@manoliTo ~/
  $ ji.py update --issue DSLE-25279 --comment comment.jira
1234876

```

- Add a comment from a markdown file:

```sh
pabgan@manoliTo ~/
  $ ji.py update --issue DSLE-25279 --comment <(md2jira.sh comment.md)
1234876

```

- Update a comment

```sh
pabgan@manoliTo ~/
  $ ji.py update --issue DSLE-25279 --comment-id 1234876 --comment <(md2jira.sh comment.md)

```

There is currently no way implemented in this tool to figure out the ID of a
comment afterwards.

## Attachments

- Upload a file:

```sh
pabgan@manoliTo ~/
  $ ji.py update --issue DSLE-21748 --attach logs.tar.gz
```

## Transition

- Select a transition to make:

```sh
pabgan@manoliTo ~
  $ ji.py update --transition --issue DSLE-24817 --comment comment.txt
More than one transition found. Please chose one by writing its ID:
ID: 701
Name: Close Issue
Description: The issue is considered finished, the resolution is correct. Issues which are closed can be reopened.
-------------------------------------------
ID: 3
Name: Reopen Issue
Description: This issue was once resolved, but the resolution was deemed incorrect. From here issues are either marked assigned or resolved.
-------------------------------------------
Type ID = 701

pabgan@manoliTo ~
  $ 

```

- Close a ticket directly:

```sh
pabgan@manoliTo ~/
  $ ji.py update --transition 'Close' --issue DSLE-23067
```

- Close a ticket adding a comment:

```sh
pabgan@manoliTo ~/
  $ ji.py update --transition 'Close' --issue DSLE-23067 --comment <(resultcat | md2jira.sh)
```

###### to be continued
